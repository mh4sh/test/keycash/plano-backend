FROM node:14-alpine

ARG CACHEBUST=1

RUN mkdir -p /home/node/aplication/node_modules && mkdir -p /home/node/aplication/dist && chown -R node:node /home/node/aplication

WORKDIR /home/node/aplication

COPY package.json yarn.* ./

USER node

RUN yarn

RUN yarn autoclean --init

RUN yarn autoclean --force

COPY --chown=node:node . .

RUN yarn build

EXPOSE 8080

CMD ["yarn", "start"]
