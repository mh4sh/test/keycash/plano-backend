import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class createTableProperty1633873867975 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'property',
        columns: [
          {
            name: 'id',
            type: 'int',
            isPrimary: true
          },
          {
            name: 'title',
            type: 'varchar',
            width: 64
          },
          {
            name: 'description',
            type: 'varchar'
          },
          {
            name: 'brokerId',
            type: 'int'
          },
          {
            name: 'latitude',
            type: 'varchar',
            width: 16
          },
          {
            name: 'longitude',
            type: 'varchar',
            width: 16
          },
          {
            name: 'addressPublicPlace',
            type: 'varchar',
            width: 128
          },
          {
            name: 'addressNumber',
            type: 'varchar',
            width: 16
          },
          {
            name: 'addressNeighborhood',
            type: 'varchar',
            width: 64
          },
          {
            name: 'addressCity',
            type: 'varchar',
            width: 32
          },
          {
            name: 'addressState',
            type: 'varchar',
            width: 32
          },
          {
            name: 'createdAt',
            type: 'timestamp',
            default: 'CURRENT_TIMESTAMP'
          },
          {
            name: 'updatedAt',
            type: 'timestamp',
            default: 'CURRENT_TIMESTAMP',
            onUpdate: 'CURRENT_TIMESTAMP'
          }
        ]
      }),
      true
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('property');
  }
}
