import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class alterPropertyTable1633924227822 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'property',
      new TableColumn({
        name: 'amount',
        type: 'int'
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('property', 'amount');
  }
}
