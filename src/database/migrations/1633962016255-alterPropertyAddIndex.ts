import { MigrationInterface, QueryRunner, TableIndex } from 'typeorm';

export class alterPropertyAddIndex1633962016255 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createIndex(
      'property',
      new TableIndex({
        name: 'IDX_PROPERTY_INFO_BEDROOMS',
        columnNames: ['infoBedrooms'],
        isUnique: false
      })
    );
    await queryRunner.createIndex(
      'property',
      new TableIndex({
        name: 'IDX_PROPERTY_INFO_PARKING_SPACE',
        columnNames: ['infoParkingSpace'],
        isUnique: false
      })
    );
    await queryRunner.createIndex(
      'property',
      new TableIndex({
        name: 'IDX_PROPERTY_INFO_SIZE',
        columnNames: ['infoSize'],
        isUnique: false
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropIndex('property', 'IDX_PROPERTY_BEDROOMS');
    await queryRunner.dropIndex('property', 'IDX_PROPERTY_PARKING_SPACE');
    await queryRunner.dropIndex('property', 'IDX_PROPERTY_SIZE');
  }
}
