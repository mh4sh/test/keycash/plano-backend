{
  "openapi": "3.0.0",
  "info": {
    "title": "Property Documentation",
    "description": "This is an API Property",
    "version": "1.0.0",
    "contact": {
      "email": "oi@marcon.run"
    }
  },
  "servers": [
    {
      "url": "https://localhost:8080/api",
      "description": "Localhost api."
    },
    {
      "url": "https://keycash.marcon.run/api",
      "description": "Sandbox test api"
    }
  ],
  "paths": {
    "/property": {
      "get": {
        "tags": [
          "Property"
        ],
        "summary": "List Properties",
        "description": "List properties per filter or no.",
        "parameters": [
          {
            "name": "bedrooms",
            "in": "query",
            "description": "Number min of bedrooms",
            "required": false,
            "schema": {
              "type": "number"
            }
          },
          {
            "name": "parkingSpace",
            "in": "query",
            "description": "Number min of parking spaces",
            "required": false,
            "schema": {
              "type": "number"
            }
          },
          {
            "name": "size",
            "in": "query",
            "description": "Sinze min of property in m²",
            "required": false,
            "schema": {
              "type": "number"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "A existent property",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "result": {
                      "type": "array",
                      "items": {
                        "$ref": "#/components/schemas/Property"
                      }
                    }
                  }
                }
              }
            }
          }
        }
      },
      "post": {
        "tags": [
          "Property"
        ],
        "summary": "Create a Property",
        "description": "Create a property.",
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "type": "object",
                "properties": {
                  "title": {
                    "type": "string",
                    "example": "Casa de 4 quartos no centro"
                  },
                  "description": {
                    "type": "string",
                    "example": "Casa recem contruida, bem cuidada para primeiro dono."
                  },
                  "location": {
                    "type": "object",
                    "properties": {
                      "latitude": {
                        "type": "string",
                        "example": "123.0000"
                      },
                      "longitude": {
                        "type": "string",
                        "example": "-12312.1231"
                      }
                    }
                  },
                  "address": {
                    "type": "object",
                    "properties": {
                      "publicPlace": {
                        "type": "string",
                        "example": "string"
                      },
                      "number": {
                        "type": "string",
                        "example": "string"
                      },
                      "neighborhood": {
                        "type": "string",
                        "example": "string"
                      },
                      "city": {
                        "type": "string",
                        "example": "string"
                      },
                      "state": {
                        "type": "string",
                        "example": "DF"
                      }
                    }
                  },
                  "broker": {
                    "type": "object",
                    "properties": {
                      "name": {
                        "type": "string",
                        "example": "Marcon Willian"
                      },
                      "email": {
                        "type": "string",
                        "example": "marcon@mh4sh.dev"
                      },
                      "phoneNumber": {
                        "type": "string",
                        "example": "66996956402"
                      }
                    }
                  },
                  "contents": {
                    "type": "array",
                    "items": {
                      "type": "object",
                      "properties": {
                        "title": {
                          "type": "string",
                          "example": "Suite Principal"
                        },
                        "description": {
                          "type": "string",
                          "example": "A suite principal tem detalhes em madeira, e a parede na cor preta."
                        },
                        "url": {
                          "type": "string",
                          "example": "https://cdn.pixabay.com/photo/2017/01/14/12/48/hotel-1979406_960_720.jpg"
                        },
                        "type": {
                          "type": "string",
                          "enum": [
                            "image",
                            "embed"
                          ],
                          "example": "image"
                        }
                      }
                    }
                  },
                  "info": {
                    "type": "object",
                    "properties": {
                      "amount": {
                        "type": "number",
                        "example": 280000
                      },
                      "bedrooms": {
                        "type": "number",
                        "example": 2
                      },
                      "suiteRooms": {
                        "type": "number",
                        "example": 1
                      },
                      "parkingSpace": {
                        "type": "number",
                        "example": 1
                      },
                      "size": {
                        "type": "number",
                        "example": 64
                      },
                      "bathRooms": {
                        "type": "number",
                        "example": 2
                      },
                      "condominiumAmount": {
                        "type": "number",
                        "example": 0
                      }
                    }
                  },
                  "extraInfo": {
                    "type": "array",
                    "items": {
                      "type": "object",
                      "properties": {
                        "key": {
                          "type": "string",
                          "example": "near"
                        },
                        "value": {
                          "type": "string",
                          "example": "trainStation"
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        },
        "responses": {
          "201": {
            "description": "Created with success"
          }
        }
      }
    },
    "/property/{propertyId}": {
      "get": {
        "tags": [
          "Property"
        ],
        "summary": "Get a Property",
        "description": "Get a property by id.",
        "parameters": [
          {
            "name": "propertyId",
            "in": "path",
            "description": "ID of property",
            "required": true,
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "A existent property",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "result": {
                      "$ref": "#/components/schemas/Property"
                    }
                  }
                }
              }
            }
          },
          "404": {
            "description": "This property had not found",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "errors": {
                      "type": "array",
                      "items": {
                        "type": "object",
                        "properties": {
                          "message": {
                            "type": "string",
                            "example": "Not found this property."
                          },
                          "key": {
                            "type": "string",
                            "example": "property.get.propertyNotExist"
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      },
      "put": {
        "tags": [
          "Property"
        ],
        "summary": "Update a Property",
        "description": "Update a property.",
        "parameters": [
          {
            "name": "propertyId",
            "in": "path",
            "description": "ID of property",
            "required": true,
            "schema": {
              "type": "string"
            }
          },
          {
            "name": "x-broker-email",
            "in": "header",
            "description": "Email of broker with create this Property",
            "required": true,
            "schema": {
              "type": "string",
              "default": "{{broker.email}}"
            }
          }
        ],
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "type": "object",
                "properties": {
                  "title": {
                    "type": "string",
                    "example": "Casa de 4 quartos no centro"
                  },
                  "description": {
                    "type": "string",
                    "example": "Casa recem contruida, bem cuidada para primeiro dono."
                  },
                  "location": {
                    "type": "object",
                    "properties": {
                      "latitude": {
                        "type": "string",
                        "example": "123.0000"
                      },
                      "longitude": {
                        "type": "string",
                        "example": "-12312.1231"
                      }
                    }
                  },
                  "address": {
                    "type": "object",
                    "properties": {
                      "publicPlace": {
                        "type": "string",
                        "example": "string"
                      },
                      "number": {
                        "type": "string",
                        "example": "string"
                      },
                      "neighborhood": {
                        "type": "string",
                        "example": "string"
                      },
                      "city": {
                        "type": "string",
                        "example": "string"
                      },
                      "state": {
                        "type": "string",
                        "example": "DF"
                      }
                    }
                  },
                  "broker": {
                    "type": "object",
                    "properties": {
                      "name": {
                        "type": "string",
                        "example": "Marcon Willian"
                      },
                      "email": {
                        "type": "string",
                        "example": "marcon@mh4sh.dev"
                      },
                      "phoneNumber": {
                        "type": "string",
                        "example": "66996956402"
                      }
                    }
                  },
                  "info": {
                    "type": "object",
                    "properties": {
                      "amount": {
                        "type": "number",
                        "example": 280000
                      },
                      "bedrooms": {
                        "type": "number",
                        "example": 2
                      },
                      "parkingSpace": {
                        "type": "number",
                        "example": 1
                      },
                      "size": {
                        "type": "number",
                        "example": 64
                      }
                    }
                  }
                }
              }
            }
          }
        },
        "responses": {
          "201": {
            "description": "Updated with success"
          },
          "404": {
            "description": "This property had not found",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "errors": {
                      "type": "array",
                      "items": {
                        "type": "object",
                        "properties": {
                          "message": {
                            "type": "string",
                            "example": "This property had not found."
                          },
                          "key": {
                            "type": "string",
                            "example": "property.update.propertyNotExist"
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          },
          "400": {
            "description": "This property had not found",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "errors": {
                      "type": "array",
                      "items": {
                        "type": "object",
                        "properties": {
                          "message": {
                            "type": "string",
                            "example": "This property had not found."
                          },
                          "key": {
                            "type": "string",
                            "example": "property.update.brokerNotExist"
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      },
      "delete": {
        "tags": [
          "Property"
        ],
        "summary": "Delete a Property",
        "description": "Create a property by id and email of broker.",
        "parameters": [
          {
            "name": "propertyId",
            "in": "path",
            "description": "ID of property",
            "required": true,
            "schema": {
              "type": "string"
            }
          },
          {
            "name": "x-broker-email",
            "in": "header",
            "description": "Email of broker with create this Property",
            "required": true,
            "schema": {
              "type": "string",
              "default": "{{broker.email}}"
            }
          }
        ],
        "responses": {
          "204": {
            "description": "Deleted"
          },
          "404": {
            "description": "This property had not found",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "errors": {
                      "type": "array",
                      "items": {
                        "type": "object",
                        "properties": {
                          "message": {
                            "type": "string",
                            "example": "This property had not found."
                          },
                          "key": {
                            "type": "string",
                            "example": "property.delete.propertyNotExist"
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          },
          "400": {
            "description": "This property had not found",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "errors": {
                      "type": "array",
                      "items": {
                        "type": "object",
                        "properties": {
                          "message": {
                            "type": "string",
                            "example": "This property had not found."
                          },
                          "key": {
                            "type": "string",
                            "example": "property.delete.brokerNotExist"
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  },
  "components": {
    "schemas": {
      "Property": {
        "type": "object",
        "properties": {
          "id": {
            "type": "number",
            "example": 1
          },
          "title": {
            "type": "string",
            "example": "Casa de 4 quartos no centro"
          },
          "description": {
            "type": "string",
            "example": "Casa recem contruida, bem cuidada para primeiro dono."
          },
          "latitude": {
            "type": "string",
            "example": "123.0000"
          },
          "longitude": {
            "type": "string",
            "example": "-12312.1231"
          },
          "addressPublicPlace": {
            "type": "string",
            "example": "string"
          },
          "addressNumber": {
            "type": "string",
            "example": "string"
          },
          "addressNeighborhood": {
            "type": "string",
            "example": "string"
          },
          "addressCity": {
            "type": "string",
            "example": "string"
          },
          "addressState": {
            "type": "string",
            "example": "DF"
          },
          "createdAt": {
            "type": "string",
            "example": "2021-10-11T00:41:13.156Z"
          },
          "updatedAt": {
            "type": "string",
            "example": "2021-10-11T00:41:55.140Z"
          },
          "broker": {
            "type": "object",
            "properties": {
              "id": {
                "type": "number",
                "example": 1
              },
              "name": {
                "type": "string",
                "example": "Marcon Willian"
              },
              "email": {
                "type": "string",
                "example": "marcon@mh4sh.dev"
              },
              "phoneNumber": {
                "type": "string",
                "example": "66996956402"
              },
              "createdAt": {
                "type": "string",
                "example": "2021-10-11T00:41:13.156Z"
              },
              "updatedAt": {
                "type": "string",
                "example": "2021-10-11T00:41:55.140Z"
              }
            }
          },
          "contents": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "id": {
                  "type": "number",
                  "example": 1
                },
                "title": {
                  "type": "string",
                  "example": "Suite Principal"
                },
                "description": {
                  "type": "string",
                  "example": "A suite principal tem detalhes em madeira, e a parede na cor preta."
                },
                "url": {
                  "type": "string",
                  "example": "https://cdn.pixabay.com/photo/2017/01/14/12/48/hotel-1979406_960_720.jpg"
                },
                "type": {
                  "type": "string",
                  "enum": [
                    "image",
                    "embed"
                  ],
                  "example": "image"
                },
                "createdAt": {
                  "type": "string",
                  "example": "2021-10-11T00:41:13.156Z"
                },
                "updatedAt": {
                  "type": "string",
                  "example": "2021-10-11T00:41:55.140Z"
                }
              }
            }
          },
          "data": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "title": {
                  "type": "string",
                  "example": "Suite Principal"
                },
                "description": {
                  "type": "string",
                  "example": "A suite principal tem detalhes em madeira, e a parede na cor preta."
                },
                "url": {
                  "type": "string",
                  "example": "https://cdn.pixabay.com/photo/2017/01/14/12/48/hotel-1979406_960_720.jpg"
                },
                "type": {
                  "type": "string",
                  "enum": [
                    "image",
                    "embed"
                  ],
                  "example": "image"
                },
                "createdAt": {
                  "type": "string",
                  "example": "2021-10-11T00:41:13.156Z"
                },
                "updatedAt": {
                  "type": "string",
                  "example": "2021-10-11T00:41:55.140Z"
                }
              }
            }
          }
        }
      }
    }
  }
}