import { PropertyContent } from '../entity/PropertyContent';
import { PropertyData } from '../entity/PropertyData';

interface ICreatePropertyDTO {
  title: string;
  description: string;
  amount: number;

  brokerId: number;
  location: {
    latitude: string;
    longitude: string;
  };

  address: {
    publicPlace: string;
    number: string;
    neighborhood: string;
    city: string;
    state: string;
  };

  info: {
    bedrooms: number;
    parkingSpace: number;
    size: number;
  };

  contents: PropertyContent[];
  data: PropertyData[];
}

export { ICreatePropertyDTO };
