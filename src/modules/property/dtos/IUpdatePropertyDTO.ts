interface IUpdatePropertyDTO {
  title?: string;
  description?: string;
  amount?: number;

  location?: {
    latitude?: string;
    longitude?: string;
  };

  address?: {
    publicPlace?: string;
    number?: string;
    neighborhood?: string;
    city?: string;
    state?: string;
  };

  info?: {
    bedrooms?: number;
    parkingSpace?: number;
    size?: number;
  };
}

export { IUpdatePropertyDTO };
