import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm';

import { Property } from './Property';

export type PropertyContentType = 'image' | 'embed';

@Entity('propertyContent')
export class PropertyContent {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ width: 64 })
  title: string;

  @Column()
  description: string;

  @Column({ width: 256 })
  url: string;

  @ManyToOne(() => Property, property => property.content, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
  })
  property: Property;

  @Column({
    type: 'enum',
    enum: ['image', 'embed'],
    default: 'image'
  })
  type: PropertyContentType;

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)'
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)'
  })
  updatedAt: Date;
}
