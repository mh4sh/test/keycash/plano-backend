import { ICreatePropertyContentDTO } from '../dtos/ICreatePropertyContentDTO';
import { PropertyContent } from '../entity/PropertyContent';

interface IPropertyContentRepository {
  createMany(data: ICreatePropertyContentDTO[]): Promise<PropertyContent[]>;
}

export { IPropertyContentRepository };
