import { getRepository, Repository } from 'typeorm';

import { ICreatePropertyDataDTO } from '../../dtos/ICreatePropertyDataDTO';
import { PropertyData } from '../../entity/PropertyData';
import { IPropertyDataRepository } from '../IPropertyDataRepository';

class PropertyDataRepository implements IPropertyDataRepository {
  private repository: Repository<PropertyData>;

  constructor() {
    this.repository = getRepository(PropertyData);
  }
  async createMany(data: ICreatePropertyDataDTO[]): Promise<PropertyData[]> {
    const dataInfo = this.repository.create(
      data.map(item => {
        return {
          key: item.key,
          value: String(item.value)
        };
      })
    );

    return this.repository.save(dataInfo);
  }
}

export { PropertyDataRepository };
