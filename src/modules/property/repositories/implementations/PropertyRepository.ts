import { FindOperator, getRepository, Repository } from 'typeorm';

import { ICreatePropertyDTO } from '../../dtos/ICreatePropertyDTO';
import { IInfoFilterDTO } from '../../dtos/IInfoFilterDTO';
import { IUpdatePropertyDTO } from '../../dtos/IUpdatePropertyDTO';
import { Property } from '../../entity/Property';
import {
  IPropertyRepository,
  PropertyExtraInformation
} from '../IPropertyRepository';

class PropertyRepository implements IPropertyRepository {
  private repository: Repository<Property>;

  constructor() {
    this.repository = getRepository(Property);
  }

  async list(
    include: PropertyExtraInformation[] = [],
    { bedrooms, parkingSpace, size }: IInfoFilterDTO = {}
  ): Promise<Property[]> {
    const properties = await this.repository.find({
      relations: include,
      where: {
        ...(bedrooms && {
          infoBedrooms: new FindOperator('moreThanOrEqual', bedrooms)
        }),
        ...(parkingSpace && {
          infoParkingSpace: new FindOperator('moreThanOrEqual', parkingSpace)
        }),
        ...(size && {
          infoSize: new FindOperator('moreThanOrEqual', size)
        })
      }
    });

    return properties;
  }

  async create({
    title,
    description,
    amount,

    brokerId,

    location: { latitude, longitude },

    address: { publicPlace, number, neighborhood, city, state },

    info: { bedrooms, parkingSpace, size },

    contents,
    data
  }: ICreatePropertyDTO): Promise<number> {
    const property = this.repository.create({
      title,
      description,
      amount,

      latitude,
      longitude,

      broker: brokerId,

      infoBedrooms: bedrooms,
      infoParkingSpace: parkingSpace,
      infoSize: size,

      addressPublicPlace: publicPlace,
      addressNumber: number,
      addressNeighborhood: neighborhood,
      addressCity: city,
      addressState: state
    });

    property.content = contents;

    property.data = data;

    await this.repository.save(property);

    return property.id;
  }

  async findById(
    propertyId: number,
    include: PropertyExtraInformation[] = []
  ): Promise<Property> {
    const property = await this.repository.findOne(
      { id: propertyId },
      {
        relations: include
      }
    );

    return property;
  }

  async updateById(
    propertyId: number,
    {
      title,
      description,
      amount,

      location,

      address,

      info
    }: IUpdatePropertyDTO,
    brokerId?: number
  ): Promise<{ affected: number }> {
    const { affected } = await this.repository.update(
      {
        id: propertyId,
        ...(brokerId && {
          broker: brokerId
        })
      },
      {
        ...(title && { title }),
        ...(description && { description }),
        ...(amount && { amount }),

        ...(location && {
          latitude: location.latitude,
          longitude: location.longitude
        }),

        ...(info && {
          infoBedrooms: info.bedrooms,
          infoParkingSpace: info.parkingSpace,
          infoSize: info.size
        }),

        ...(address && {
          addressPublicPlace: address.publicPlace,
          addressNumber: address.number,
          addressNeighborhood: address.neighborhood,
          addressCity: address.city,
          addressState: address.state
        })
      }
    );
    return { affected };
  }

  async deleteById(
    propertyId: number,
    brokerId: number = null
  ): Promise<{ affected: number }> {
    const { affected } = await this.repository.delete({
      id: propertyId,
      ...(brokerId && {
        broker: brokerId
      })
    });
    return { affected };
  }
}

export { PropertyRepository };
