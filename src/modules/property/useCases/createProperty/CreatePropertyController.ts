import { celebrate, Joi, Segments } from 'celebrate';
import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { CreatePropertyUseCase } from './CreatePropertyUseCase';

class CreatePropertyController {
  async handle(request: Request, response: Response): Promise<Response> {
    const {
      title,
      description,
      location,
      address,
      broker,
      contents,
      info,
      extraInfo
    } = request.body;

    const createPropertyUseCase = container.resolve(CreatePropertyUseCase);

    await createPropertyUseCase.execute({
      title,
      description,
      location,
      address,
      broker,
      contents,
      info,
      extraInfo
    });

    return response.status(201).send();
  }

  validate = celebrate(
    {
      [Segments.BODY]: Joi.object().keys({
        title: Joi.string().max(64).required(),
        description: Joi.string().required(),
        location: Joi.object()
          .keys({
            latitude: Joi.string().max(16).required(),
            longitude: Joi.string().max(16).required()
          })
          .required(),
        address: Joi.object()
          .keys({
            publicPlace: Joi.string().max(128).required(),
            number: Joi.string().max(16).required(),
            neighborhood: Joi.string().max(64).required(),
            city: Joi.string().max(32).required(),
            state: Joi.string().max(32).required()
          })
          .required(),
        broker: Joi.object()
          .keys({
            name: Joi.string().max(64).required(),
            email: Joi.string().email().required(),
            phoneNumber: Joi.string().max(16).required()
          })
          .required(),
        contents: Joi.array().items(
          Joi.object().keys({
            title: Joi.string().max(64).required(),
            description: Joi.string().required(),
            url: Joi.string().max(256).required(),
            type: Joi.string().valid('image', 'embed')
          })
        ),
        info: Joi.object().keys({
          amount: Joi.number().required(),
          bedrooms: Joi.number().required(),
          suiteRooms: Joi.number().required(),
          parkingSpace: Joi.number().required(),
          size: Joi.number().required(),
          bathRooms: Joi.number().required(),
          condominiumAmount: Joi.number().default(0)
        }),
        extraInfo: Joi.array().items(
          Joi.object().keys({
            key: Joi.string().required(),
            value: Joi.string().required()
          })
        )
      })
    },
    {
      abortEarly: false
    }
  );
}

export { CreatePropertyController };
