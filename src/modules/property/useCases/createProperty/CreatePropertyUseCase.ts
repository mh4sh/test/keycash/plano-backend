/* eslint-disable no-async-promise-executor */
import { inject, injectable } from 'tsyringe';

import { IBrokerRepository } from '../../../broker/repositories/IBrokerRepository';
import { IPropertyContentRepository } from '../../repositories/IPropertyContentRepository';
import { IPropertyDataRepository } from '../../repositories/IPropertyDataRepository';
import { IPropertyRepository } from '../../repositories/IPropertyRepository';

interface IRequest {
  title: string;
  description: string;

  location: {
    latitude: string;
    longitude: string;
  };

  address: {
    publicPlace: string;
    number: string;
    neighborhood: string;
    city: string;
    state: string;
  };

  broker: {
    name: string;
    email: string;
    phoneNumber: string;
  };

  contents: {
    title: string;
    description: string;
    url: string;
    type: 'image' | 'embed';
  }[];

  info: {
    amount: number;
    bedrooms: number;
    suiteRooms: number;
    parkingSpace: number;
    size: number;
    bathRooms: number;
    condominiumAmount: number;
  };
  extraInfo: {
    key: string;
    value: string;
  }[];
}

@injectable()
class CreatePropertyUseCase {
  constructor(
    @inject('BrokerRepository')
    private brokerRepository: IBrokerRepository,
    @inject('PropertyRepository')
    private propertyRepository: IPropertyRepository,
    @inject('PropertyContentRepository')
    private propertyContentRepository: IPropertyContentRepository,
    @inject('PropertyDataRepository')
    private propertyDataRepository: IPropertyDataRepository
  ) {}

  async execute({
    title,
    description,
    location,
    broker,
    address,
    contents,
    info,
    extraInfo
  }: IRequest): Promise<void> {
    let brokerId = 0;

    const brokerAlreadyExist = await this.brokerRepository.findByEmail(
      broker.email
    );

    if (!brokerAlreadyExist) {
      const newBroker = await this.brokerRepository.create(broker);

      brokerId = newBroker;
    } else {
      brokerId = brokerAlreadyExist.id;
    }

    const contentsSaved = await this.propertyContentRepository.createMany(
      contents
    );

    const dataSaved = await this.propertyDataRepository.createMany([
      { key: 'bathRooms', value: info.bathRooms },
      { key: 'condominiumAmount', value: info.condominiumAmount },
      { key: 'suiteRooms', value: info.suiteRooms },
      ...extraInfo
    ]);

    await this.propertyRepository.create({
      title,
      description,
      amount: info.amount,
      location,
      brokerId,
      address,
      info: {
        bedrooms: info.bedrooms,
        parkingSpace: info.parkingSpace,
        size: info.size
      },
      contents: contentsSaved,
      data: dataSaved
    });
  }
}

export { CreatePropertyUseCase };
