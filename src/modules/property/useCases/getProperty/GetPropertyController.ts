import { celebrate, Joi, Segments } from 'celebrate';
import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { GetPropertyUseCase } from './GetPropertyUseCase';

class GetPropertyController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { propertyId } = request.params;

    const getPropertyUseCase = container.resolve(GetPropertyUseCase);

    const property = await getPropertyUseCase.execute({
      propertyId: Number(propertyId)
    });

    return response.status(200).json({ result: property });
  }

  validate = celebrate(
    {
      [Segments.PARAMS]: Joi.object().keys({
        propertyId: Joi.number().required()
      })
    },
    {
      abortEarly: false
    }
  );
}

export { GetPropertyController };
