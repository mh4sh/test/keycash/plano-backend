import { inject, injectable } from 'tsyringe';

import { Property } from '../../entity/Property';
import { IPropertyRepository } from '../../repositories/IPropertyRepository';

interface IRequest {
  bedrooms?: number;
  parkingSpace?: number;
  size?: number;
}

interface IInfo {
  [key: string]: string | number;
}

interface IProperty extends Property {
  info: IInfo;
}
@injectable()
class ListPropertyUseCase {
  constructor(
    @inject('PropertyRepository')
    private propertyRepository: IPropertyRepository
  ) {}

  async execute({
    bedrooms,
    parkingSpace,
    size
  }: IRequest): Promise<IProperty[]> {
    const properties = await this.propertyRepository.list(
      ['broker', 'content', 'data'],
      {
        bedrooms,
        parkingSpace,
        size
      }
    );

    return properties.map(property => {
      const info = (property.data || []).reduce(
        (prevItem, actualItem) => {
          return {
            ...prevItem,
            [actualItem.key]:
              Number(actualItem.value) || actualItem.value === '0'
                ? Number(actualItem.value)
                : actualItem.value
          };
        },
        {
          bedrooms: property.infoBedrooms,
          parkingSpace: property.infoParkingSpace,
          size: property.infoSize
        } as IInfo
      );

      delete property.data;
      delete property.infoBedrooms;
      delete property.infoParkingSpace;
      delete property.infoSize;

      return {
        ...property,
        info
      };
    });
  }
}

export { ListPropertyUseCase };
