import { Router } from 'express';
import * as swaggerUi from 'swagger-ui-express';

import * as swaggerFile from '../docs/swagger.json';
import { PropertyRoutes } from './property.Routes';

const Routes = Router();

Routes.use('/api/docs', swaggerUi.serve, swaggerUi.setup(swaggerFile));

Routes.use('/api/property', PropertyRoutes);

Routes.get('/', (request, response) => {
  return response.json({
    result: {
      test: 'This route is ok, and is root path'
    }
  });
});

export default Routes;
