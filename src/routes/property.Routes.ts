import { Router } from 'express';

import { CreatePropertyController } from '../modules/property/useCases/createProperty/CreatePropertyController';
import { DeletePropertyController } from '../modules/property/useCases/deleteProperty/DeletePropertyController';
import { GetPropertyController } from '../modules/property/useCases/getProperty/GetPropertyController';
import { ListPropertyController } from '../modules/property/useCases/listProperty/ListPropertyController';
import { UpdatePropertyController } from '../modules/property/useCases/updateProperty/UpdatePropertyController';

const createPropertyController = new CreatePropertyController();
const deletePropertyController = new DeletePropertyController();
const getPropertyController = new GetPropertyController();
const listPropertyController = new ListPropertyController();
const updatePropertyController = new UpdatePropertyController();

const PropertyRoutes = Router();

PropertyRoutes.get(
  '/',
  listPropertyController.validate,
  listPropertyController.handle
);

PropertyRoutes.post(
  '/',
  createPropertyController.validate,
  createPropertyController.handle
);

PropertyRoutes.get(
  '/:propertyId',
  getPropertyController.validate,
  getPropertyController.handle
);

PropertyRoutes.put(
  '/:propertyId',
  updatePropertyController.validate,
  updatePropertyController.handle
);

PropertyRoutes.delete(
  '/:propertyId',
  deletePropertyController.validate,
  deletePropertyController.handle
);

export { PropertyRoutes };
