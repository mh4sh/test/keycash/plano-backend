import { container } from 'tsyringe';

import { IBrokerRepository } from '../../modules/broker/repositories/IBrokerRepository';
import { BrokerRepository } from '../../modules/broker/repositories/implementations/BrokerRepository';
import { PropertyContentRepository } from '../../modules/property/repositories/implementations/PropertyContentRepository';
import { PropertyDataRepository } from '../../modules/property/repositories/implementations/PropertyDataRepository';
import { PropertyRepository } from '../../modules/property/repositories/implementations/PropertyRepository';
import { IPropertyContentRepository } from '../../modules/property/repositories/IPropertyContentRepository';
import { IPropertyDataRepository } from '../../modules/property/repositories/IPropertyDataRepository';
import { IPropertyRepository } from '../../modules/property/repositories/IPropertyRepository';

container.registerSingleton<IBrokerRepository>(
  'BrokerRepository',
  BrokerRepository
);

container.registerSingleton<IPropertyRepository>(
  'PropertyRepository',
  PropertyRepository
);

container.registerSingleton<IPropertyContentRepository>(
  'PropertyContentRepository',
  PropertyContentRepository
);

container.registerSingleton<IPropertyDataRepository>(
  'PropertyDataRepository',
  PropertyDataRepository
);
